#ifndef CutsGridHotspotStability_H
#define CutsGridHotspotStability_H

#include "EventBase.h"

class CutsGridHotspotStability {

public:
    CutsGridHotspotStability(EventBase* eventBase);
    ~CutsGridHotspotStability();
    bool GridHotspotStabilityCutsOK(int pulseID);
    bool PulseWidth(int pulseID);
    bool PulseArea(int pulseID);
    bool RandomTrigger();
    bool NoLargePulses();
    bool Classification(int pulseID);
    bool CutPreTrigger(int pulseID);
    bool Coincidence(int pulseID);
    bool PositionChiSq(int pulseID);

private:
    EventBase* m_event;
};

#endif
