#ifndef GridHotspotStability_H
#define GridHotspotStability_H

#include "Analysis.h"

#include "CutsGridHotspotStability.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class GridHotspotStability : public Analysis {

public:
    GridHotspotStability();
    ~GridHotspotStability();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsGridHotspotStability* m_gridCuts;
    ConfigSvc* m_conf;
    SkimSvc* m_skim;
};

#endif
