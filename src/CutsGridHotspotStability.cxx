#include "CutsGridHotspotStability.h"
#include "ConfigSvc.h"

CutsGridHotspotStability::CutsGridHotspotStability(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsGridHotspotStability::~CutsGridHotspotStability()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsGridHotspotStability::GridHotspotStabilityCutsOK(int pulseID)
{
    // List of common cuts for this analysis into one cut
    return RandomTrigger() && CutPreTrigger(pulseID) && Classification(pulseID);
}

// Cut on pulse width (defined as AFT95-AFT5)
bool CutsGridHotspotStability::PulseWidth(int pulseID)
{
    float width = (*m_event->m_tpcPulses)->areaFractionTime95_ns[pulseID] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[pulseID];
    return ((width >= 80.) && (width <= 1700.));
}


// Cut on pulse area
bool CutsGridHotspotStability::PulseArea(int pulseID)
{
    float area = (*m_event->m_tpcPulses)->pulseArea_phd[pulseID];
    return ((area >= 20.) && (area <= 400.));
}


// Random triggers only
bool CutsGridHotspotStability::RandomTrigger()
{
    return (*m_event->m_eventHeader)->triggerType == 32;
}

// Cut events with large pulses
bool CutsGridHotspotStability::NoLargePulses()
{
    return (*m_event->m_tpcPulses)->maxPulseArea_phd < 1000;
}

// Cut on pulse classification
bool CutsGridHotspotStability::Classification(int pulseID)
{
    return ((*m_event->m_tpcPulses)->classification[pulseID] == "SE" || (*m_event->m_tpcPulses)->classification[pulseID] == "S2");
}

bool CutsGridHotspotStability::CutPreTrigger(int pulseID)
{
    return ((*m_event->m_tpcPulses)->pulseStartTime_ns[pulseID] > 0);
}

bool CutsGridHotspotStability::Coincidence(int pulseID)
{
    return ((*m_event->m_tpcPulses)->coincidence[pulseID] > 2);
}

bool CutsGridHotspotStability::PositionChiSq(int pulseID)
{
    return ((*m_event->m_tpcPulses)->s2XYchi2[pulseID] < 1.);
}
