#include "GridHotspotStability.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsGridHotspotStability.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include <TEllipse.h>
#include <TBox.h>
#include <cmath>
#include <TH1.h>
#include <TString.h>

// Constructor
GridHotspotStability::GridHotspotStability()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("eventHeader");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("GridHotspotStability Analysis");

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();

    // Setup the analysis specific cuts.
    m_gridCuts = new CutsGridHotspotStability(m_event);
}

// Destructor
GridHotspotStability::~GridHotspotStability()
{
    delete m_gridCuts;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void GridHotspotStability::Initialize()
{
    INFO("Initializing GridHotspotStability Analysis");
}

// Execute() - Called once per event.
void GridHotspotStability::Execute()
{
    // Event info
    int run_group = m_conf->configFileVarMap["run_group"];

    double firstTriggerTime;
    if      (run_group == 0) firstTriggerTime = 1634511521.; // ER only
    else if (run_group == 1) firstTriggerTime = 1637989583.; // Nov 9kV runs
    else if (run_group == 2) firstTriggerTime = 1639453528.; // 6621
    else if (run_group == 3) firstTriggerTime = 1639474372.; // 6622
    
    double triggerEndTime;
    if      (run_group == 0) triggerEndTime =  50400.;
    else if (run_group == 1) triggerEndTime = 135000.;
    else if (run_group == 2) triggerEndTime =  20700.;
    else if (run_group == 3) triggerEndTime =  20400.;
    
    double triggerTime_s   = (*m_event->m_eventHeader)->triggerTimeStamp_s-firstTriggerTime;
    double triggerTime     = triggerTime_s + (*m_event->m_eventHeader)->triggerTimeStamp_ns*1e-9;
    float timeBinSize      = 5.;
    int numBins            = triggerEndTime/timeBinSize + 1;
    
    vector<float> hotspotXpos;
    vector<float> hotspotYpos;
    float radius = 10.;
    if (run_group == 0 || run_group == 1) { 
         hotspotXpos = {-65., -34.};
         hotspotYpos = {  9.,  44.5};
    } else if (run_group == 2 || run_group == 3) {
        hotspotXpos = {-65., 68.};
        hotspotYpos = {  1., -2.};
    }

    for (int pulseID=0; pulseID<(*m_event->m_tpcPulses)->nPulses; pulseID++) {
        float xPos = (*m_event->m_tpcPulses)->s2Xposition_cm[pulseID];
        float yPos = (*m_event->m_tpcPulses)->s2Yposition_cm[pulseID];
        float pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[pulseID];
        float pulseWidth = ((*m_event->m_tpcPulses)->areaFractionTime95_ns[pulseID] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[pulseID]);
        string classification = (*m_event->m_tpcPulses)->classification[pulseID];
        
        // Plot area of all SE/S2s
        if (classification == "SE" or classification == "S2") {
            float pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[pulseID];
            m_hists->BookFillHist("AreaTimeAll", numBins, 0., triggerEndTime, 5000, 0., 7., triggerTime, TMath::Log10(pulseArea));
            m_hists->BookFillHist("WidthArea", 500, 2., 5.5, 500, 0.5, 7., TMath::Log10(pulseWidth), TMath::Log10(pulseArea));
            m_hists->BookFillHist("XY_Position", 500, -100., 100., 500, -100., 100., xPos, yPos);
        }

        // Random triggers only
        if (m_gridCuts->GridHotspotStabilityCutsOK(pulseID)) {
            m_hists->BookFillHist("AreaTimeAll_rand", numBins, 0., triggerEndTime, 5000, 0., 7., triggerTime, TMath::Log10(pulseArea));
            m_hists->BookFillHist("WidthArea_Rand", 500, 2., 5.5, 500, 0.5, 7., TMath::Log10(pulseWidth), TMath::Log10(pulseArea));
            m_hists->BookFillHist("XY_Position_Rand", 500, -100., 100., 500, -100., 100., xPos, yPos);
        }
    
        // Zoom in on hotspots
        for (int hi=0; hi<hotspotXpos.size(); hi++) {
            float hsx = hotspotXpos[hi];  float hsy = hotspotYpos[hi];
            
            // select all within a circle centered on hotspot
            float dist2 = pow((xPos - hsx), 2) + pow((yPos - hsy), 2);
            if (dist2 <= pow(radius, 2)) {
                string name = "Hotspot" + to_string(hi);
                string areaHistName = "AreaTime" + name;

                // plot all SE + S2 in hotspot
                m_hists->BookFillHist(areaHistName, numBins, 0., triggerEndTime, 5000, 0., 7., triggerTime, TMath::Log10(pulseArea));
            
                // random triggers only
                if (m_gridCuts->GridHotspotStabilityCutsOK(pulseID)) {
                    m_hists->BookFillHist(areaHistName + "_rand", numBins, 0., triggerEndTime, 5000, 0., 7., triggerTime, TMath::Log10(pulseArea));
                }
            }
        
            // fill bg hist
            // use 180 deg location for bg subtraction (match light yield)
            float bgx = -hotspotXpos[hi]; float bgy = -hotspotYpos[hi];
            float bgdist2 = pow((xPos - bgx), 2) + pow((yPos - bgy), 2);
            if (bgdist2 <= pow(radius, 2)) {
                string bgname = "Background" + to_string(hi);
                string abgname = "AreaTime" + bgname;
                m_hists->BookFillHist(abgname, numBins, 0., triggerEndTime, 5000, 0., 7., triggerTime, TMath::Log10(pulseArea));

                if (m_gridCuts->GridHotspotStabilityCutsOK(pulseID)) {
                    m_hists->BookFillHist(abgname + "_rand", numBins, 0., triggerEndTime, 5000, 0., 7., triggerTime, TMath::Log10(pulseArea));
                }
            }
        }
    }
    // random trigger livetime
    if (m_gridCuts->RandomTrigger()) {
        m_hists->BookFillHist("Livetime_rand", numBins, 0., triggerEndTime, triggerTime);
    }

}

// Finalize() - Called once after event loop.
void GridHotspotStability::Finalize()
{
    INFO("Finalizing GridHotspotStability Analysis");
    auto livetimeHist = m_hists->GetHistFromMap("Livetime_rand");
    livetimeHist->Scale(2.5/1000.); // 2.5 ms per trigger
}
